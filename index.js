var ffmpeg = require('fluent-ffmpeg');
var request = require('request');
var fs = require('fs');
var Hose = require('flowr-hose');

var hose = new Hose({
  name: 'transcode',
  port: process.env.PORT || 0,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

var exchange = process.env.FILEX;

console.log("TTN.TRANSCO is running...")

hose.process('audio', process.env.CONCURRENCY || 1,function(job, done){
	console.log("transcode job arrived!")
	transcode(job.data, function(transcodedFile){
		upload(transcodedFile, exchange,function(uploadedFile){
			removeTmpFile(transcodedFile)
			done(null, uploadedFile)
		})
	});
});

function removeTmpFile(file){
	fs.unlink(file, function (err) {
		if (err) throw err;
		console.log('successfully deleted '+ file);
	});
}

function transcode(data, cb) {
  console.log("ffmpeg: workin on it..")
	//data.file
	//data.format
	//data.audioQuality
	//data.bitrate
	var filename = data.file.split('/').pop()
	var output = '/tmp/'+filename+'.'+data.format

	var command = ffmpeg(data.file)
		//.audioCodec('libvorbis')
		.format(data.format)
		.audioQuality(5)
		.noVideo()
		command.save(output);
    command.on('end',function(){
      console.log("ffmpeg: i'm done!")
		    cb(output);
    });
}

function upload(file, exchange, cb){
	console.log("request: working on it...")
	var formData = {
		file: fs.createReadStream(file)
	};

	request.post({url: exchange, formData: formData}, function callback(err, httpResponse, body) {
		if (err) {
			return console.error('upload failed:', err);
		 }
		console.log("request: I'm done!")
		console.log(body)
		cb(JSON.parse(body)[0]);
	});
}
